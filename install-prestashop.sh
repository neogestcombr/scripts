# exemplo:
# 
# ./install-prestashop.sh 1.7.6.5 parada loja neo000gest suporte neo0@0Gest
# 

version=$1
user=$2
database=$3
pass_db=$4
account=$5
passwd=$6

./scripts/clean-database.sh $user\_$database $pass_db $user\_$database

echo " - instalando PrestaShop v$version";
echo " - limpando a public_html";
cd public_html/
rm -rf *

echo " - baixando o prestashop v$version";
wget https://download.prestashop.com/download/releases/prestashop_$version.zip
unzip -qq -o prestashop_$version.zip
rm -rf index.php Install_PrestaShop.html prestashop_$version.zip

echo " - descompactando o prestashop";
unzip prestashop.zip
rm -rf prestashop.zip
cd install/

echo " - executando a instalação";
php index_cli.php --step=all --language=br --timezone=America/Sao_Paulo --domain=$user.paradadigital.com.br --db_server=localhost --db_name=$user\_$database --db_user=$user\_$database --db_password=$pass_db --prefix=ps_ --engine=InnoDB --name='NeoGest Desenvolvimento' --activity=0 --country=br --firstname=NeoGest --lastname=Suporte --password=$passwd --email=$account@neogest.com.br --license=0 --newsletter=0 --ssl=1 --rewrite=1
cd ../
rm -rf install/
mv admin/ painel/

echo " - corrige a permissão dos arquivos";
chown -R $user:$user *
cd ../
echo " - instalação finalizada com sucesso";
