# instruções:
# 
# este script deve ficar no mesmo nível da pasta public_html
# 

git clone git@bitbucket.org:neogestcombr/scripts.git
chown dev:dev -R scripts/
chmod +x scripts/*
echo "AddHandler application/x-httpd-php73 .php" > public_html/.htaccess
chown dev:dev public_html/.htaccess
./scripts/install-prestashop.sh 1.7.7.2 dev loja neo000Gest suporte neo0@0Gest
rm -rf scripts/

